﻿using UnityEngine;
using System.Collections;

public class LevelController : MonoBehaviour {

    public AudioClip levelClip;         // The Main Music Clip of the level
    public bool activeGUI;              // Should the GUI being shown in this level?
    public PlayerController player;     // PlayerController script reference


    void Awake() {

        if (!player) {
            Debug.LogWarning("No PlayerController GameObject Attached.");
        }

        if (levelClip) {
            AudioController ac = FindObjectOfType<AudioController>();

            if (ac) {
                ac.playTrack(levelClip, true);
            }
            else {
                Debug.LogError("No AudioController found in the scene, but an AudioClip is assigned to be played.");
            }
        }

        GUIController GUI = FindObjectOfType<GUIController>();

        if (GUI) {
            GUI.HUD.SetActive(activeGUI);
        }
        else {
            Debug.LogError("No GUIController found in the scene.");
        }
    }
}
