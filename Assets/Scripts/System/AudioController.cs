﻿using UnityEngine;
using System.Collections;

public class AudioController : MonoBehaviour {

    private static AudioSource musicPlayer;
    public AudioClip[] songList;
    private float targetVolume;
    private float _maxVolume;
    public float maxVolume{
        get { return _maxVolume; }
        set { _maxVolume = value;}
    }
    private bool isClipFading;
    [Range(0,1)]
    public float fadingSpeed;

	// Use this for initialization
	void Awake () {

        musicPlayer = this.GetComponent<AudioSource>();
        targetVolume = musicPlayer.volume = maxVolume = 1;
        isClipFading = false;
        fadingSpeed = 0.1f;
    }

    void Update() {

        if (isClipFading) {
            fadeTrack();
        }
    }

    public void playTrack(AudioClip clip, bool faded) {
        if (faded) {
            isClipFading = true;
            targetVolume = 0;
        }

        musicPlayer.clip = clip;
        musicPlayer.Play();
    }

    public void playTrack(int trackIndex, bool faded) {
        //Cheking if the given index correspond to a valid clip (position into the array)
        if (songList.Length > trackIndex +1) {
            if (faded) {
                isClipFading = true;
                targetVolume = 0;
                musicPlayer.clip = songList[trackIndex];
            }
            else {
                musicPlayer.clip = songList[trackIndex];
                musicPlayer.Play();
            }
        }
    }

    public void playTrack(string trackName, bool faded) {
        
        int i = 0;
        bool found = false;
        int track = 0;

        //Cheking if the given index correspond to a valid clip (position into the array)
        while (i < songList.Length && !found) {
            if (songList[i].name == trackName) { 
                track = i;
                found = true;
             }
            i++;
        }

        if (found) {
            playTrack(track, faded);
        }
    }

    

    private void fadeTrack() {
        if (targetVolume < musicPlayer.volume) {
            musicPlayer.volume -= fadingSpeed;
            if (musicPlayer.volume <= 0) {
                targetVolume = maxVolume;
                musicPlayer.Play();
            }

        }
        else if (targetVolume > musicPlayer.volume) {
            musicPlayer.volume += fadingSpeed;
            if (musicPlayer.volume >= maxVolume) {
                isClipFading = false;
            }
        }
    }
}
