﻿using UnityEngine;
using UnityEngine.EventSystems;
#if UNITY_5_5
using UnityEngine.AI;
#endif

public abstract class PlayerController : MonoBehaviour {

    public SaveData playerSaveData;             // Reference to the save data asset containing the player's starting position.
    protected Interactable currentInteractable;                       // The interactable that is currently being headed towards.
    public const string startingPositionKey = "starting position";  // The key used to retrieve the starting position from the playerSaveData.

    protected bool handleInput = true;
    public float inputHoldDelay = 0.5f;                             // How long after reaching an interactable before input is allowed again.
    protected WaitForSeconds inputHoldWait;                           // The WaitForSeconds used to make the user wait before input is handled again.


    public abstract void OnWalkableLayerClick(BaseEventData data);
    public abstract void OnUnWalkableLayerClick(BaseEventData data);
    public abstract void OnReachableInteractableClick(Interactable interactable);
    public abstract void OnImmediateInteractableClick(Interactable interactable);
}
