﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;
#if UNITY_5_5
using UnityEngine.AI;
#endif

[RequireComponent (typeof(PlayerMovement2D))]
[RequireComponent (typeof(PlayerAnimation2D))]
public class PlayerController2D : PlayerController {

    public PolygonCollider2D walkableLayer;
    
    private PlayerMovement2D playerMovement;
    private PlayerAnimation2D playerAnimation;

    private void Start() {
        if (!walkableLayer) {
            Debug.LogError("No walkable Layer reference found.");
            enabled = false;
            return;
        }

        playerMovement = GetComponent<PlayerMovement2D>();
        playerAnimation = GetComponent<PlayerAnimation2D>();

        //Load the starting position from the save data and find the transform from the starting position's name.
        string startingPositionName = "";
        playerSaveData.Load(startingPositionKey, ref startingPositionName);
        Transform startingPosition = StartingPosition.FindStartingPosition(startingPositionName);

        //Set the player's position based on the starting position.
        if (startingPosition) {
            transform.position = startingPosition.position;
        }
        else {
            Debug.LogWarning("No starting position found with name " + startingPositionName);
        }
        

        //Create the wait based on the delay.
        inputHoldWait = new WaitForSeconds(inputHoldDelay);
    }

    private void Update() {

        //handle interaction
        if (currentInteractable && !playerMovement.isMoving) {
            Debug.Log("Interacting");
            currentInteractable.Interact();
            currentInteractable = null;
            StartCoroutine(WaitForInteraction());
        }

        //handleAnimation
        playerAnimation.handleAnimation(playerMovement.destinationPosition.position, playerMovement.isMoving, playerMovement.velocity);
    }

    /*OnClick Events*/
    // This function is called by the EventTrigger on the scene's walkable surface when it is clicked on.
    public override void OnWalkableLayerClick(BaseEventData data) {

        //If the handle input flag is set to false then do nothing.
        if (!handleInput)
            return;

        //The player doesn't click on an interactable GameObject
        currentInteractable = null;


        //This function needs information about a click so cast the BaseEventData to a PointerEventData.
        PointerEventData pData = (PointerEventData)data;
        //Set the player destination to clicked position
        playerMovement.destinationPosition.position = pData.pointerCurrentRaycast.screenPosition;
    }

    //Player clicks/taps on the non walkable surface
    public override void OnUnWalkableLayerClick(BaseEventData data) {

        //If the handle input flag is set to false then do nothing.
        if (!handleInput)
            return;

        //The player didn't click on an interactable GameObject
        currentInteractable = null;

        //Here we have two approachs. 
        //First option, if the player clicks in the non_walkable layer, we can make the character says something like "I can't go there".
        //Second option, we can calculate the walkable layer nearest point to the player click point, and move the character there. 

        //second option   
        PointerEventData pData = (PointerEventData)data;
        RaycastHit2D hit = Physics2D.Raycast(pData.pointerCurrentRaycast.worldPosition, Vector2.down, Mathf.Infinity, 1 << LayerMask.NameToLayer("Walkable"));

        //We found the nearest point to the player click point in the walkable layer
        if (hit) {
            playerMovement.destinationPosition.position = hit.point;
        }
        else {
            //Make the character says that the point clicked/taped by the player is imposible to reach
            //TODO conversation system
            print("I can't go there.");
        }
    }

    //This function is called by the EventTrigger on an Interactable, the Interactable component is passed into it.
    public override void OnReachableInteractableClick(Interactable interactable) {
        Debug.Log("Click on Intertable Level Item" + interactable.gameObject.transform.name);
        //If the handle input flag is set to false then do nothing.
        if (!handleInput)
            return;

        //Store the interactble that was clicked on.
        currentInteractable = interactable;

        //Set the destination to the interaction location of the interactable.
        playerMovement.destinationPosition.position = currentInteractable.interactionLocation.position;
    }

    //This function is called by the EventTrigger on an Interactable, and trigger instantaneously
    public override void OnImmediateInteractableClick(Interactable interactable) {
        Debug.Log("Click on Intertable GUI Item" + interactable.gameObject.transform.name);
        //If the handle input flag is set to false then do nothing.
        if (!handleInput)
            return;

        //Interact onClick
        interactable.Interact();
    }

    private IEnumerator WaitForInteraction() {

        //As soon as the wait starts, input should no longer be accepted.
        handleInput = false;

        //Wait for the normal pause on interaction.
        yield return inputHoldWait;

        //Until the animator is in a state with the Locomotion tag, wait.
        while (!playerAnimation.isInLocomotion()) {
            yield return null;
        }

        //Now input can be accepted again.
        handleInput = true;
    }
}
