﻿using UnityEngine;

public class CharacterScaler : SpriteScaler {

    public PlayerMovement2D character;      //Reference of the movement script
    
    protected override void Awake() {

        base.Awake();

        if (!character) {
            Debug.LogError("No character GameObject Attached.");
            enabled = false;
            return;
        }
    }

    protected override void Update() {

        base.Update();
        //Multiply the character's speed by the same scale factor as scale.
        character.modifyCurrentSpeed(sizeFactor);
    }
}



