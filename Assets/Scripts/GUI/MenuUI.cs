﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using UnityEngine.UI;

public class MenuUI : MonoBehaviour {

    public GameObject initialPanel;                   // Initialy opened menu panel
    private GameObject currentPanel;                  // Currently opened menu panel
    private GameObject previouslySelectedOption;      // The option selected in the previous panel to keep track of 

    public void OnEnable() {

        if (initialPanel == null)
            return;

        OpenPanel(initialPanel);
    }

    public void OpenPanel(GameObject panel) {

        if (currentPanel == panel) {
            return;
        }
            
        previouslySelectedOption = EventSystem.current.currentSelectedGameObject;

        CloseCurrent();
        currentPanel = panel;
        currentPanel.SetActive(true);


        GameObject go = FindFirstEnabledSelectable(panel);

        SetSelected(go);

    }

    public void CloseCurrent() {

        if (currentPanel == null)
            return;

        SetSelected(previouslySelectedOption);
        currentPanel.SetActive(false);
        currentPanel = null;
    }

    static GameObject FindFirstEnabledSelectable(GameObject gameObject) {

        GameObject go = null;
        var selectables = gameObject.GetComponentsInChildren<Selectable>(true);
        foreach (var selectable in selectables) {
            if (selectable.IsActive() && selectable.IsInteractable()) {
                go = selectable.gameObject;
                break;
            }
        }
        return go;
    }


   

    private void SetSelected(GameObject panel) {
        EventSystem.current.SetSelectedGameObject(panel);
    }
}
