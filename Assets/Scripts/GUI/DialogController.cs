﻿using UnityEngine;

public class DialogController : MonoBehaviour {

    public VIDE_Assign initialDialogue;
    public bool loadSceneOnDialogEnd;
    public string sceneToLoad = "Scene to load name";
    public string startingPosition = "Character's starting position in the scene";
    private DialogueUI diagUI;               //Reference to our diagUI script for quick access
    private VIDE_Assign currentDialog;

    // Use this for initialization
    void Start () {

        diagUI = FindObjectOfType<DialogueUI>();
        if (diagUI == null) {
            Debug.LogError("No DialogueUI found in the scene.");
            enabled = false;
            return;
        }

        // If there is an intro dialog, play it at start
        if (initialDialogue != null) {
            currentDialog = initialDialogue;
            TryInteract();
        }
    }
	
	// Update is called once per frame
	void Update () {

        //Interact with NPCs when hitting spacebar
        if (Input.anyKeyDown && currentDialog != null) {
            TryInteract();
        }
    }

    private void TryInteract() {
        //Lets grab the NPC's DialogueAssign script...
        if (currentDialog != null) {
            if (!diagUI.dialogue.isLoaded) {
                //... and use it to begin the conversation
                diagUI.Begin(currentDialog);
            }
            else {

                //If conversation already began, let's just progress through it
                diagUI.NextNode();

                if (!diagUI.dialogue.isLoaded && loadSceneOnDialogEnd) {

                    FindObjectOfType<SceneController>().FadeAndLoadScene(sceneToLoad, startingPosition);
                    currentDialog = null;

                }
            }
        }
    }
}
