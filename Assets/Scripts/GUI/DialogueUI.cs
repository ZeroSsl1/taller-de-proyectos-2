﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Text;

//This script will handle everything related to dialogue interface
//It will use a VIDE_Data component to load dialogues and retrieve node data
//to draw the text for the dialogue

public class DialogueUI : MonoBehaviour {
    
    [System.NonSerialized]
    public VIDE_Data dialogue; //All you need for everything to work is to have a variable of this type in your custom UI script

    //These are just references to UI components in the scene
    public Text npcText;
    public Text npcName;
    public Text playerText;
    public GameObject itemText;
    public GameObject uiContainer;
    [Range(0.01f, 0.1f)]
    public float textSpeed = 0.01f;
	
	//We'll use these later
    bool gotItem = false;
	bool animatingText = false;

    //We'll be using this to store the current player dialogue options
    private List<Text> currentOptions = new List<Text>();

    //Here I'm assigning the variable a new component of its required type
    void Start(){

        dialogue = gameObject.AddComponent<VIDE_Data>();
        //Remember you can also manually add the VIDE_Data script as a component in the Inspector, 
        //then drag&drop it on your 'dialogue' variable slot
    }

    //Show-hide stuff will happen here
    void Update(){
		
        //Lets just store the Node Data variable for the sake of fewer words
        var data = dialogue.nodeData;
		
        //We'll be disabling the entire UI if there aren't any loaded conversations
        if (!dialogue.isLoaded){
            uiContainer.SetActive(false);
        }
        else {

            uiContainer.SetActive(true);
            //Player-NPC conversation text will be visible depending on whose turn it is
            playerText.transform.parent.gameObject.SetActive(data.currentIsPlayer);
            npcText.transform.parent.gameObject.SetActive(!data.currentIsPlayer);

            //Color the Player options. Blue for the selected one
            for (int i = 0; i < currentOptions.Count; i++) {
                currentOptions[i].color = Color.black;
                if (i == data.selectedOption)
                    currentOptions[i].color = Color.blue;
            }

            //Scroll through Player dialogue options
            if (Input.GetKeyDown(KeyCode.S)) {
                if (data.selectedOption < currentOptions.Count - 1)
                    data.selectedOption++;
            }

            if (Input.GetKeyDown(KeyCode.W)) {
                if (data.selectedOption > 0)
                    data.selectedOption--;
            }		
        }
    }

    //This begins the conversation (Called by examplePlayer script)
    public void Begin(VIDE_Assign diagToLoad) {
        //First step is to call BeginDialogue, passing the required 'DialogueAssign' component 
        //This will store the first Node data in dialogue.nodeData
        dialogue.BeginDialogue(diagToLoad);

        //Let's clean the NPC text variables
        npcText.text = "";
        npcName.text = "";

        //If we already talked to this NPC, lets modify the start of the conversation
        if (dialogue.assigned.interactionCount > 0 && gotItem){
            string name = dialogue.assigned.dialogueName;
            switch (name) {
                case "Crazy Cap":
                    dialogue.nodeData = dialogue.SetNode(17);
                    break;
            }
        }

        //Everytime dialogue.nodeData gets updated, we update our UI with the new data
        UpdateUI();
    }

    //This will handle what happens when we want next message to appear 
	//(Also called by examplePlayer script)
    public void NextNode(){
        var data = dialogue.nodeData;
		
		//Let's not go forward if text is currently being animated, but let's speed it up.
		if (animatingText) {
            animatingText = false;
            return;
        }

        //Check to see if there's extraData and if so, we do stuff
        if (!data.currentIsPlayer && data.extraData != ""){
            DoAction(data);
        }
        //Otherwise, let's just move on with the next Player or NPC dialogue
        else {
            //This will update the dialogue.nodeData with the next Node's data
            dialogue.Next();
        }

        UpdateUI();
    }

    public void UpdateUI() {

        var data = dialogue.nodeData;	

        //If we've reached the end, let's end everything.
        if (data.isEnd){
    		//This is called when we have reached the end of the conversation
       		dialogue.EndDialogue();
            return;
        }

        //If this new Node is a Player Node, set the selectable comments offered by the Node
        if (data.currentIsPlayer) {
            SetOptions(data.playerComments);
        }
        //If it's an NPC Node, let's just update NPC's text
        else{
			if (npcText.text != data.npcComment[data.npcCommentIndex]){
				npcText.text = "";
				StartCoroutine(AnimateText());	
			}	
			npcName.text = data.tag;
        }
    }

    //This uses the returned string[] from nodeData.playerComments to create the UIs for each comment
    //It first cleans, then it instantiates new options
    public void SetOptions(string[] opts){
		//Destroy the current options
        foreach (Text op in currentOptions)
            Destroy(op.gameObject);
		
		//Clean the variable
        currentOptions = new List<Text>();
		
		//Create the options
        for (int i = 0; i < opts.Length; i++){
			//This is just one way of creating endless options for Unity's UI class
			//Normally, you'd have an absolute number of options and you wouldn't have the need of doing this
            GameObject newOp = Instantiate(playerText.gameObject, playerText.transform.position, Quaternion.identity) as GameObject;
            newOp.SetActive(true);
            newOp.transform.SetParent(playerText.transform.parent, true);
            //newOp.GetComponent<RectTransform>().anchoredPosition = new Vector2(playerText.transform.position.x, playerText.transform.position.y - (playerText.transform.position.y * i));
            newOp.GetComponent<Text>().text = opts[i];
            currentOptions.Add(newOp.GetComponent<Text>());
        }
    }

    public void DoAction(VIDE_Data.NodeData data){
        switch(data.extraData){
            case "item":
				//npcCommentIndex refers to the current NPC's comment when there are many in a single Node (when you use <br>)
                if (data.npcCommentIndex == 1) {
                    if (!itemText.activeSelf){
                        itemText.SetActive(true);
                        gotItem = true;
                    } else{
                        itemText.SetActive(false);
                        dialogue.Next();
                    }
                } else {
                    dialogue.Next();
                }

                break;

            case "insanity":
				//This will override the Dialogue's Start Node and use this one instead
                dialogue.assigned.overrideStartNode = 16; 
                dialogue.Next();
                break;
        }
    }
	
	//Very simple text animation, not optimal
    //Use StringBuilder for better performance
	public IEnumerator AnimateText(){
        var data = dialogue.nodeData;
		animatingText = true;	
		
        if (!data.currentIsPlayer){

            StringBuilder npcCurrentComment = new StringBuilder(data.npcComment[data.npcCommentIndex]);
            int currentCharPos = 0;
            List<TagInfo> tags = new List<TagInfo>();

            while (currentCharPos < npcCurrentComment.Length && animatingText) {
                char letterToAdd = npcCurrentComment[currentCharPos];
                
                //Rich text tags parsing
                if (letterToAdd.Equals('<') && tags.Count == 0) {  //Rich Text tag found
                    tags = ParseRichTextTags(npcCurrentComment, currentCharPos);
                }

                StringBuilder strToAdd = new StringBuilder(letterToAdd.ToString());
                //There are tags to handle
                if (tags.Count > 0) {
                  
                    string prefixes = "";
                    string sufixes = "";

                    for (int i = 0; i < tags.Count; i++) {

                        if(currentCharPos >= tags[i].prefixStartPosition && currentCharPos <= tags[i].prefixEndPosition)
                            currentCharPos = tags[i].prefixEndPosition+1;

                        if (tags[i].prefixEndPosition < currentCharPos && tags[i].sufixStartPosition > currentCharPos) {
                            prefixes += tags[i].prefix;
                            sufixes = tags[i].sufix + sufixes;
                        }

                        if (currentCharPos >= tags[i].sufixStartPosition && currentCharPos <= tags[i].sufixEndPosition) {
                            currentCharPos = tags[i].sufixEndPosition+1;
                            tags.RemoveAt(i);
                        }
                    }

                    strToAdd = (tags.Count>0 &&  !npcCurrentComment[currentCharPos].Equals('<')) ?
                        new StringBuilder(prefixes + npcCurrentComment[currentCharPos] + sufixes) :
                        new StringBuilder(prefixes +  "" + sufixes);
                }

                npcText.text = npcText.text.Insert(npcText.text.Length, strToAdd.ToString()); //Actual text updates here


                yield return new WaitForSeconds(textSpeed);

                currentCharPos++;
            }
		} 
		
		npcText.text = data.npcComment[data.npcCommentIndex]; //And here		
		animatingText = false;		
	}

    //TODO Method test, needs exception handling
    private List<TagInfo> ParseRichTextTags(StringBuilder npcCurrentComment, int currentCharPos) {
        //aux vars
        int i = currentCharPos;
        char tagLetter = ' ';
        int currentTag = -1;
        List<TagInfo> tags = new List<TagInfo>();

        //Store the tags with their respective start/end position info
        while (i < npcCurrentComment.Length && animatingText) {

            tagLetter = npcCurrentComment[i];

            // Tag opening
            if (tagLetter.Equals('<')) {
                if (npcCurrentComment[i + 1].Equals('/')) { //close tag 
                    tags[currentTag].sufixStartPosition = i;
                    tags[currentTag].closeTag = true;
                }
                else {  //open tag
                    TagInfo tag = new TagInfo();
                    tag.prefixStartPosition = i;
                    tag.openTag = true;
                    tags.Add(tag);
                    currentTag++;
                }
            }

            // Tag closing
            if (tagLetter.Equals('>')) {
                if (tags[currentTag].openTag) {     //open tag
                    tags[currentTag].prefix.Append(tagLetter);
                    tags[currentTag].prefixEndPosition = i;
                    tags[currentTag].openTag = false;
                }

                if (tags[currentTag].closeTag) {    //close tag
                    tags[currentTag].sufix.Append(tagLetter);
                    tags[currentTag].sufixEndPosition = i;
                    tags[currentTag].closeTag = false;

                    //Here tagInfo would be completely formed
                    Debug.Log("Tag nº: " + currentTag);
                    Debug.Log("Tag prefix: " + tags[currentTag].prefix);
                    Debug.Log("Tag prefix Start pos: " + tags[currentTag].prefixStartPosition);
                    Debug.Log("Tag prefix End pos: " + tags[currentTag].prefixEndPosition);
                    Debug.Log("Tag sufix: " + tags[currentTag].sufix);
                    Debug.Log("Tag sufix Start pos: " + tags[currentTag].sufixStartPosition);
                    Debug.Log("Tag sufix End pos: " + tags[currentTag].sufixEndPosition);

                    currentTag--;
                }

                //We deal with all Rich Text Tags...
                if (currentTag == -1) {
                    break;
                }
            }

            //getting prefix and sufix values
            //prefix
            if (tags[currentTag].openTag)
                tags[currentTag].prefix.Append(tagLetter);

            //sufix
            if (tags[currentTag].closeTag)
                tags[currentTag].sufix.Append(tagLetter);

            //Character between Rich Text Tags position
            i++;
        }

        return tags;
    }

    class TagInfo {
        public StringBuilder prefix;
        public StringBuilder sufix;

        public int prefixStartPosition;
        public int prefixEndPosition;
        public int sufixStartPosition;
        public int sufixEndPosition;

        public bool closeTag;
        public bool openTag;

        public TagInfo() {
            prefix = new StringBuilder();
            sufix = new StringBuilder();
            closeTag = false;
            openTag = false;
        }
    }
}
