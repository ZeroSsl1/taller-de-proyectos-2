﻿using UnityEngine;
using System.Collections;

public class GUIController : MonoBehaviour {

    public GameObject HUD;
    public GameObject inventory;
    public GameObject map;

    

    public void displayHUD() {
        HUD.SetActive(!HUD.activeSelf);
        Debug.Log("HUD");
    }

    public void displayInventory() {
        inventory.SetActive(!inventory.activeSelf);
        Debug.Log("inventory");
    }

    public void displayMap() {
        map.SetActive(!map.activeSelf);
        Debug.Log("map");
    }
}
