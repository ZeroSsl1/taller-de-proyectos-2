﻿using UnityEngine;
using System.Collections;

public class MenuController : MonoBehaviour {

    public void NewGame(string sceneName) {
        SceneController sc = FindObjectOfType<SceneController>();
        sc.FadeAndLoadScene(sceneName);
    }


}
