﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MapController : MonoBehaviour {

    public List<MapLocation>  mapLocations;

    public void activateLocation(string locationToActivateName, bool activate) {

        MapLocation locationToActivate = mapLocations.Find(lc => lc.gameObject.name == locationToActivateName);
        locationToActivate.setActive(activate);
    }
}
